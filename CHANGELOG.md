## [2.1.3] - 2024-10-23
### Fixed
- default settings value

## [2.1.2] - 2024-10-23
### Fixed
- default settings value

## [2.1.1] - 2024-01-26
### Fixed
- package padding

## [2.1.0] - 2022-08-30
### Added
- de_DE translations

## [2.0.1] - 2022-08-12
### Changed
- Polish translations

## [2.0.0] - 2022-05-16
### Added
- new packing algorithm: 3d box

## [1.4.0] - 2022-01-11
### Fixed
- wp-woocommerce-shipping library compatibility

## [1.3.3] - 2021-10-21
### Changed
- texts improvements

## [1.3.2] - 2021-01-11
### Fixed
- packages field description

## [1.3.1] - 2021-01-11
### Added
- additional description to shipping boxes field

## [1.2.10] - 2020-12-28
### Fixed
- sprintf formatting

## [1.2.9] - 2020-01-28
### Changed
- units translations moved from packages to items

## [1.2.8] - 2020-01-28
### Fixed
- packages codes

## [1.2.7] - 2020-01-24
### Fixed
- packages units conversions

## [1.2.6] - 2020-01-24
### Fixed
- packer

## [1.2.5] - 2020-01-24
### Fixed
- packer

## [1.2.4] - 2020-01-11
### Added
- translations

## [1.2.3] - 2020-01-11
### Fixed
- assets path

## [1.2.2] - 2020-01-10
### Fixed
- assets for boxes

## [1.2.1] - 2020-01-10
### Fixed
- text domain in some translations
- FedexProVendor namespace

## [1.2.0] - 2020-01-09
### Added
- PackerFactory
### Changed
- Better packer settings
### Fixed
- Bugs in WooCommerceShippingBuilder

## [1.1.0] - 2019-12-27
### Added
- Packer
- ShippingBoxes field
- WooCommerceShippingBuilder that can pack
- ProMethodFieldsFactory - factory that can create ShippingBoxes field

## [1.0.0] - 2019-12-19
### Added
- initial version
